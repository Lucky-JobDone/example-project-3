<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/Lucky-JobDone/example-project-3/compare/1.0.0...1.0.1) (2020-04-27)


### Bug Fixes

* **gitlab-ci:** also did the --tags on the other branches ([f23e0e1](https://gitlab.com/Lucky-JobDone/example-project-3/commit/f23e0e1))
* **gitlab-ci:** removed fetch prune and replaced with fetch --tags ([5735f1b](https://gitlab.com/Lucky-JobDone/example-project-3/commit/5735f1b))

<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/Lucky-JobDone/example-project-3/compare/1.0.0...1.0.1) (2020-04-22)


### Bug Fixes

* **gitlab-ci:** also did the --tags on the other branches ([f23e0e1](https://gitlab.com/Lucky-JobDone/example-project-3/commit/f23e0e1))
* **gitlab-ci:** removed fetch prune and replaced with fetch --tags ([5735f1b](https://gitlab.com/Lucky-JobDone/example-project-3/commit/5735f1b))

<a name="1.0.1-alpha.0"></a>
## [1.0.1-alpha.0](https://gitlab.com/Lucky-JobDone/example-project-3/compare/1.0.0...1.0.1-alpha.0) (2020-04-22)


### Bug Fixes

* **gitlab-ci:** also did the --tags on the other branches ([f23e0e1](https://gitlab.com/Lucky-JobDone/example-project-3/commit/f23e0e1))
* **gitlab-ci:** removed fetch prune and replaced with fetch --tags ([5735f1b](https://gitlab.com/Lucky-JobDone/example-project-3/commit/5735f1b))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/Lucky-JobDone/example-project-3/compare/1.0.0...1.0.1-alpha.0) (2020-04-22)


### Features

* **app:** initial release ([f7a0b5d](https://gitlab.com/Lucky-JobDone/example-project-3/commit/f7a0b5d))

